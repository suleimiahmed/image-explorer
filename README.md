# Image Explorer


It's important to manage costs for your GitLab instance. Without maintenance, it's easy for storage and data transfer costs to grow out of control. GitLab [container registry cleanup policies](https://docs.gitlab.com/ee/development/packages/cleanup_policies.html#container-registry) helps to reduce your cost of storage.

Another way to reduce storage and data transfer costs is to make **how** you build your container images more efficient. This project exposes a GitLab CI component that utilizes https://github.com/wagoodman/dive under the hood to help discover how much space is wasted in your Docker/OCI image build stage and prevent builds that waste too much space (up to a predefined threshold).

For more details on how the "Image wastage/efficiency " is calculated please refer to the underlying tool's [doc](https://github.com/wagoodman/dive?tab=readme-ov-file#basic-features).

## How To Use

To add the Image explorer to an existing pipeline:

1. Add a `.dive-ci.yaml` rules file in the root of your project with the threshold rules for the image explorer, e.g:

```yaml
rules:
  # If the efficiency is measured below X%, mark as failed.
  # Expressed as a ratio between 0-1.
  lowestEfficiency: 0.95

  # If the amount of wasted space is at least X or larger than X, mark as failed.
  # Expressed in B, KB, MB, and GB.
  highestWastedBytes: 20MB

  # If the amount of wasted space makes up for X% or more of the image, mark as failed.
  # Note: the base image layer is NOT included in the total image size.
  # Expressed as a ratio between 0-1; fails if the threshold is met or crossed.
  highestUserWastedPercent: 0.20
```

2. Add the image explorer component to a stage (e.g `test`) in your `.gitlab-ci.yml` file:

```yaml
include:
  - component: gitlab.com/suleimiahmed/image-explorer/image-explorer@main
    inputs:
      stage: test
      # Container file name to be assesed (defaults to `Dockerfile`)
      #container_file: "Dockerfile"
      # The file path to to be find container file (defaults to `.`)
      #container_file_path: "."
```

3. Start your pipeline. A sample output looks like:

```sh
Analyzing image...
  efficiency: 99.8266 %
  wastedBytes: 864322 bytes (864 kB)
  userWastedPercent: 0.2466 %
Inefficient Files:
Count  Wasted Space  File Path
    2        436 kB  /etc/ssl/certs/ca-certificates.crt
    5        348 kB  /lib/apk/db/installed
    5         72 kB  /lib/apk/db/scripts.tar
    2        2.4 kB  /etc/passwd
    3        2.1 kB  /etc/group
    2        1.4 kB  /etc/group-
    5         924 B  /lib/apk/db/triggers
    2         875 B  /etc/shadow
    5         776 B  /etc/apk/world
    2          86 B  /etc/shells
    2          23 B  /etc/subgid
    2          23 B  /etc/subuid
    2           0 B  /usr/bin/lzma
    2           0 B  /usr/bin/lzcat
    2           0 B  /usr/bin/unxz
    2           0 B  /usr/bin/xzcat
    2           0 B  /lib/apk/db/lock
    2           0 B  /lib/apk/exec
    2           0 B  /var/cache/apk
    2           0 B  /usr/bin/unlzma
Results:
  PASS: highestUserWastedPercent
  PASS: highestWastedBytes
  PASS: lowestEfficiency
Result:PASS [Total:3] [Passed:3] [Failed:0] [Warn:0] [Skipped:0]
```
